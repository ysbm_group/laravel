<?php
/**
 * Created by PhpStorm.
 * User: alexk
 * Date: 07.03.16
 * Time: 15:39
 */

namespace App\Helpers;

use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Lookup;
use ApaiIO\ApaiIO;

class AmazonHelper
{

    protected $channel;

    /**
     * AmazonHelper constructor.
     */
    public function __construct($channel)
    {
        //check type ??? amazon
        $this->channel = $channel;
    }


    protected function resolveCountry() {
        $domain = parse_url($this->channel->url, PHP_URL_HOST);
        $firstLevel = 'amazon';
        if(($pos = strpos($domain, $firstLevel)) !== false) {
            return substr($domain, $pos + strlen($firstLevel) + 1);
        } else {
            throw new \Exception('Not Amazon domain');
        }
    }

    protected function getConfiguration() {
        $conf = new GenericConfiguration();
        $country = $this->resolveCountry();
        $conf->setCountry($country);

        $conf
            ->setAssociateTag(env('AMAZON_ASSOCIATE_TAG'))
            ->setAccessKey(env('AMAZON_ACCESS_KEY'))
            ->setSecretKey(env('AMAZON_SECRET_KEY'));

        /*
        switch($country) {
            case 'it':
                $conf
                    ->setAccessKey('AKIAJ2J4ROC4GYIWCMMQ')
                    ->setSecretKey('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')
                    ->setAssociateTag('jo8th6-21');
                break;
            default:
                throw new \Exception('Not supported Amazon country: '.$country);
        }
        */
        return $conf;
    }


    protected function parseItem($item) {
        $data = [
            'asin' => (string)$item->ASIN,
            'rank' => (integer)$item->SalesRank
        ];

        $data['new_count'] = (integer)($item->OfferSummary->TotalNew);
        if (isset($item->OfferSummary->LowestNewPrice)) {
            $object = $item->OfferSummary->LowestNewPrice;
            $data['currency'] = (string)$object->CurrencyCode;
            $data['new_lowest_price'] = (string)$object->Amount / 100;
        }

        $data['used_count'] = (integer)($item->OfferSummary->TotalUsed);
        if (isset($item->OfferSummary->LowestUsedPrice)) {
            $object = $item->OfferSummary->LowestUsedPrice;
            $data['currency'] = (string)$object->CurrencyCode;
            $data['used_lowest_price'] = (string)$object->Amount / 100;
        }
        return $data;
    }


    public function getBookData($id, $type = Lookup::TYPE_ISBN) {
        //maybe send multiple ISBN for some book and get data for rank from another request
        $conf = $this->getConfiguration();
        $conf
            ->setResponseTransformer('\ApaiIO\ResponseTransformer\XmlToSimpleXmlObject');

        $apaiIO = new ApaiIO($conf);
        $lookup = new Lookup();
        $lookup->setIdType($type);
        $lookup->setResponseGroup(['Offers', 'SalesRank']); //http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_ResponseGroupsList.html
        $lookup->setItemId($id);
        $formattedResponse = $apaiIO->runOperation($lookup);

        try {
            $items = $formattedResponse->Items->Item;
            if ($items === null || !$items->count()) return false;
        } catch (\Exception $e) {
            return false;
        }


        foreach($items as $item) {
            if ($item->OfferSummary->count()) {
                return $this->parseItem($item);
            }
        }

        return false;
    }

}