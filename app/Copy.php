<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Copy extends Model
{
    protected $fillable = ['book_id', 'year', 'condition_id', 'location_id', 'notes'];

    public function setYearAttribute($value)
    {
        $this->attributes['year'] = $value !== '' ? $value : null;
    }

    public function book()
    {
        return $this->belongsTo('App\Book');
    }

    public function purchase()
    {
        return $this->hasOne('App\Purchase');
    }

    public function sale()
    {
        return $this->hasOne('App\Sale');
    }

    public function condition()
    {
        return $this->belongsTo('App\Condition');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function getYear() {
        return $this->year ? $this->year : $this->book->year;
    }

    public function getLabelAttribute() {
        return $this->book->label;
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($model) {
            foreach($model->images as $image) {
                //delete as model with clearing attaches
                $image->delete();
            }
        });
    }

    public function setLocationIdAttribute($value)
    {
        $this->attributes['location_id'] = $value !== '' ? $value : null;
    }

}
