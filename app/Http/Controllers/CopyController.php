<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Copy;
use App\Book;
use App\Image;
use DB;
use App\Purchase;
use App\Sale;

class CopyController extends Controller
{

    protected function modelValidate($request, $id = null) {
        $book = is_numeric($request->book_id) ? Book::find($request->book_id) : null;
        $rules = [
            'book_id' => 'required|exists:books,id',
            'year' => 'integer|max:'.date('Y').($book && $book->year ? '|min:'.$book->year : 0),
            'condition_id' => 'required|exists:conditions,id',
            'location_id' => 'exists:locations,id',
            'notes' => 'max:255',
        ];
        //Add images validation
        if ($request->hasFile('images')) {
            foreach($request->file('images') as $index => $image) {
                $rules['images.' . $index] = 'image';
            }
        }

        $messages = [
            'year.min' => 'The year be at least as book year (:min).',
        ];

        $this->validate($request, $rules, $messages);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //multiple filters for incoming data
        $models = Copy::with([
            'book',
            'book.author',
            'book.language',
            'book.publisher',
            'condition',
            'location',
        ])
            ->leftJoin('sales', 'sales.copy_id', '=', 'copies.id')->select('copies.*')
            ->whereNull('sales.id') //clear sold
            ->orWhere('sales.status', '<>', Sale::STATUS_SOLD)
            ->paginate(15);
        return view('copy.index', [
            'models' => $models
        ]);
    }

    public function indexSold(Request $request) {
        $query = Copy::with([
            'sale',
            'sale.channel',
            'sale.currency',
            'book',
            'book.author',
            'book.language',
            'book.publisher'
        ])
            ->select('copies.*')
            ->leftJoin('sales', 'sales.copy_id', '=', 'copies.id')
            ->where('sales.status', Sale::STATUS_SOLD);
        if ($request->has('channel')) {
            $ids = Channel::parseChannels($request->channel);
            $query->whereIn('sales.channel_id', $ids);
        }

        $models = $query->paginate();
        return view('copy.indexSold', [
            'models' => $models
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('copy.create', [
            'book' => old('book_id') === null ? null : Book::findOrFail(old('book_id'))
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->modelValidate($request);
        DB::beginTransaction();
        try {
            //copy
            $model = Copy::create($request->all());
            //images
            if ($request->hasFile('images')) {
                foreach($request->file('images') as $index => $uploadImage) {
                    if ($uploadImage->isValid()) {
                        $image = new Image();
                        $image->filename = $uploadImage->getClientOriginalName();
                        $model->images()->save($image);
                        //$image
                        $path = $image->genPath();
                        makePath(dirname($path));
                        $uploadImage->move(dirname($path), basename($path));
                    }
                }
            }
            DB::commit();
        } catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        Session::flash('success', 'Successfully added!');
        return redirect()->route('copy.show', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Copy::findOrFail($id);
        return view('copy.show', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Copy::findOrFail($id);
        return view('copy.edit', [
            'model' => $model,
            'book' => old('book_id') === null ? $model->book : Book::findOrFail(old('book_id'))
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Copy::findOrFail($id);
        if ($request->has('delete_image')) {
            $image = $model->images()->findOrFail($request->delete_image);
            $image->delete();
            return redirect()->to($this->getRedirectUrl())
                ->withInput($request->input());
        }

        $this->modelValidate($request, $id);
        DB::beginTransaction();
        try {
            //copy
            $model->fill($request->all())->save();
            //images
            if ($request->hasFile('images')) {
                foreach($request->file('images') as $index => $uploadImage) {
                    if ($uploadImage->isValid()) {
                        $image = new Image();
                        $image->filename = $uploadImage->getClientOriginalName();
                        $model->images()->save($image);
                        //$image
                        $path = $image->genPath();
                        makePath(dirname($path));
                        $uploadImage->move(dirname($path), basename($path));
                    }
                }
            }
            DB::commit();
        } catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        Session::flash('success', 'Successfully edit!');
        return redirect()->route('copy.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Copy::findOrFail($id);
        $model->delete();
        Session::flash('success', 'Successfully deleted!');
        return redirect()->route('copy.index');
    }


    public function purchaseEdit($id) {
        $copy = Copy::findOrFail($id);
        $model = $copy->purchase ? $copy->purchase : new Purchase();

        return view('copy.purchaseEdit', [
            'model' => $model,
            'copy' => $copy
        ]);
    }

    public function purchaseUpdate(Request $request, $id) {
        $copy = Copy::findOrFail($id);
        $this->validate($request, [
            'date' => 'required|date',
            'location' => 'required|string|max:255',
            'seller' => 'string|max:255',
            'currency_code' => 'required|exists:currencies,code',
            'purchase_price' => 'required|numeric',
            'shipping_price' => 'numeric',
        ]);
        if(!$copy->purchase) {
            $model = new Purchase();
            $model->copy_id = $copy->id;
        } else {
            $model = $copy->purchase;
        }
        $model->fill($request->all())->save();
        Session::flash('success', 'Successfully edit!');
        return redirect()->route('copy.show', ['id' => $copy->id]);
    }



    public function saleEdit($id) {
        $copy = Copy::findOrFail($id);
        $model = $copy->sale ? $copy->sale : new Sale();

        return view('copy.saleEdit', [
            'model' => $model,
            'copy' => $copy
        ]);
    }

    public function saleUpdate(Request $request, $id) {
        $copy = Copy::findOrFail($id);
        $this->validate($request, [
            'status' => 'required|integer|in:'.implode(',', array_keys(Sale::statusList())),

            'shipped' => 'required|boolean',
            'courier' => 'required_if:shipped,1|string|max:255',
            'tracking_number' => 'string|max:255',

            'first_day_on_sale' => 'required_if:status,'.Sale::STATUS_ON_SALE.'|date',

            'sale_date' => 'required_if:status,'.Sale::STATUS_SOLD.'|date',
            'channel_id' => 'required_if:status,'.Sale::STATUS_SOLD.'|exists:channels,id',
            'currency_code' => 'required_if:status,'.Sale::STATUS_SOLD.'|exists:currencies,code',
            'commissions' => 'required_if:status,'.Sale::STATUS_SOLD.'|numeric',
            'paypal' => 'required_if:status,'.Sale::STATUS_SOLD.'|boolean',
            'paypal_fees' => 'required_if:paypal,1|numeric',
            'sale_price' => 'required_if:status,'.Sale::STATUS_SOLD.'|numeric',
            'shipping_price' => 'required_if:status,'.Sale::STATUS_SOLD.'|numeric',
            'actual_shipping_price' => 'required_if:status,'.Sale::STATUS_SOLD.'|numeric',
        ]);
        if(!$copy->sale) {
            $model = new Sale();
            $model->copy_id = $copy->id;
        } else {
            $model = $copy->sale;
        }
        $model->fill($request->all())->save();
        Session::flash('success', 'Successfully edit!');
        return redirect()->route('copy.show', ['id' => $copy->id]);
    }

}
