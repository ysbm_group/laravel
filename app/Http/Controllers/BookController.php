<?php

namespace App\Http\Controllers;

use App\Channel;
use App\ChannelGroup;
use App\Helpers\MathEval;
use App\Publisher;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Book;
use App\Image;
use Session;
use DB;
use View;
use App\Author;
use Yajra\Datatables\Datatables;


class BookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('book.index');
    }

    public function indexData(Request $request) {
        $models = Book::with(['author', 'language', 'publisher'])->select('books.*')
            ->leftJoin('authors', 'books.author_id', '=', 'authors.id')
            ->leftJoin('languages', 'books.language_id', '=', 'languages.id')
            ->leftJoin('publishers', 'books.publisher_id', '=', 'publishers.id');

        $dataTables = Datatables::of($models)
            ->addColumn('actions', function($model) {
                return view('book._actions', ['model'=> $model])->render();
            })
            //not require fields
            ->addColumn('author_name', function($model) {
                return $model->author ? $model->author->name : null;
            })
            ->addColumn('publisher_name', function($model) {
                return $model->publisher ? $model->publisher->name : null;
            })
            ->addColumn('language_name', function($model) {
                return $model->language ? $model->language->name : null;
            })
            ->addColumn('copies_count', function($model) {
                return $model->copies->count();
            })
            ->addColumn('remain_copies_count', function($model) {
                return $model->remainCopies->count();
            });

        dataTablesStrictSearch($dataTables, 'books.id');
        dataTablesStrictSearch($dataTables, 'authors.name', 'authors.id');
        dataTablesStrictSearch($dataTables, 'languages.name', 'languages.id');
        dataTablesStrictSearch($dataTables, 'publishers.name', 'publishers.id');

        return $dataTables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = old('author_id');
        if (is_numeric($author)) {
            $author = ['key' => $author, 'value' => Author::findOrFail($author)->name];
        } elseif(is_string($author)) {
            $author = ['key' => $author, 'value' => $author];
        }

        $publisher = old('publisher_id');
        if (is_numeric($publisher)) {
            $publisher = ['key' => $publisher, 'value' => Publisher::findOrFail($publisher)->name];
        } elseif(is_string($publisher)) {
            $publisher = ['key' => $publisher, 'value' => $publisher];
        }

        return view('book.create', [
            'author' => $author,
            'publisher' => $publisher,
            'tags' => old('tags', [])
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->modelValidate($request);
        DB::beginTransaction();
        try {
            //book
            $model = new Book($request->except(['author_id', 'publisher_id',]));

            if (!empty($request->author_id)) {
                $model->author_id = is_numeric($request->author_id) ? $request->author_id : Author::create(['name' => $request->author_id])->id;
            }
            if (!empty($request->publisher_id)) {
                $model->publisher_id = is_numeric($request->publisher_id) ? $request->publisher_id : Publisher::create(['name' => $request->publisher_id])->id;
            }
            $model->save();
            //tags
            if ($request->has('tags')) {
                $model->retag($request->tags);
            }
            //images
            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $index => $uploadImage) {
                    if ($uploadImage->isValid()) {
                        $image = new Image();
                        $image->filename = $uploadImage->getClientOriginalName();
                        $model->images()->save($image);
                        //$image
                        $path = $image->genPath();
                        makePath(dirname($path));
                        $uploadImage->move(dirname($path), basename($path));
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        Session::flash('success', 'Successfully added!');
        return redirect()->route('book.show', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Book::findOrFail($id);
        return view('book.show', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Book::findOrFail($id);

        $author = old('author_id');
        if ($author === null && $model->author_id !== null) {
            $author = ['key' => $model->author_id, 'value' => $model->author->name];
        } elseif (is_numeric($author)) {
            $author = ['key' => $author, 'value' => Author::findOrFail($author)->name];
        } elseif(is_string($author)) {
            $author = ['key' => $author, 'value' => $author];
        }

        $publisher = old('publisher_id');
        if ($publisher === null && $model->publisher_id !== null) {
            $publisher = ['key' => $model->publisher_id, 'value' => $model->publisher->name];
        } elseif (is_numeric($publisher)) {
            $publisher = ['key' => $publisher, 'value' => Publisher::findOrFail($publisher)->name];
        } elseif(is_string($publisher)) {
            $publisher = ['key' => $publisher, 'value' => $publisher];
        }

        $tags = old('tags');
        if ($tags === null) {
            $tags = $model->tagNames();
        }

        return view('book.edit', [
            'model' => $model,
            'author' => $author,
            'publisher' => $publisher,
            'tags' => $tags
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Book::findOrFail($id);
        if ($request->has('delete_image')) {
            $image = $model->images()->findOrFail($request->delete_image);
            $image->delete();
            return redirect()->to($this->getRedirectUrl())
                ->withInput($request->input());
        }
        $this->modelValidate($request, $id);
        DB::beginTransaction();
        try {
            //book
            $model->fill($request->all($request->except(['author_id', 'publisher_id',])));

            if (!empty($request->author_id)) {
                $model->author_id = is_numeric($request->author_id) ? $request->author_id : Author::create(['name' => $request->author_id])->id;
            } else {
                $model->author_id = null;
            }

            if (!empty($request->publisher_id)) {
                $model->publisher_id = is_numeric($request->publisher_id) ? $request->publisher_id : Publisher::create(['name' => $request->publisher_id])->id;
            } else {
                $model->publisher_id = null;
            }
            $model->save();

            //tags
            if ($request->has('tags')) {
                $model->retag($request->tags);
            } else {
                $model->untag();
            }
            //images
            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $index => $uploadImage) {
                    if ($uploadImage->isValid()) {
                        $image = new Image();
                        $image->filename = $uploadImage->getClientOriginalName();
                        $model->images()->save($image);
                        //$image
                        $path = $image->genPath();
                        makePath(dirname($path));
                        $uploadImage->move(dirname($path), basename($path));
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        Session::flash('success', 'Successfully edit!');
        return redirect()->route('book.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Book::findOrFail($id);
        try {
            $model->delete();
            Session::flash('success', 'Successfully deleted!');
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == '23000') {
                Session::flash('error', 'Can\'t delete item used in other models');
            } else
                throw $e;
        }
        return redirect()->route('book.index');
    }

    public function addCopy($id) {
        return view('copy.create', [
            'book' => Book::findOrFail($id)
        ]);
    }

    protected function modelValidate($request, $id = null)
    {
        //http://www.amazon.com/gp/seller/asin-upc-isbn-info.html Add ASIN
        $rules = [
            'isbn_10' => 'size:10|regex:/^[0-9X]+$/|unique:books' . ($id ? ',id,' . $id : ''),
            'isbn_13' => 'size:13|regex:/^[0-9]+$/|unique:books' . ($id ? ',id,' . $id : ''),
            'asin' => 'size:10|regex:/^[0-9A-Z]+$/|unique:books' . ($id ? ',id,' . $id : ''),
            'currency_code' => 'required_with:cover_price|exists:currencies,code',
            'cover_price' => 'numeric',
            'language_id' => 'exists:languages,id',
            'publisher_id' => is_numeric($request->publisher_id) ? 'exists:publishers,id' : 'min:2|max:255|unique:publishers,name',
            'series' => 'max:255',
            'special_type_id' => 'boolean',
            'year' => 'integer|min:1000|max:' . date('Y'),
            'author_id' => is_numeric($request->author_id) ? 'exists:authors,id' : 'min:2|max:255|unique:authors,name',
            'title' => 'required|max:255',
            'origin_year' => 'integer|min:0|max:' . date('Y'),
            'origin_title' => 'max:255',
            'notes' => 'max:255',
        ];

        //Add images validation
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $index => $image) {
                $rules['images.' . $index] = 'image';
            }
        }
        $this->validate($request, $rules);
    }

    public function autoComplete(Request $request)
    {
        $models = Book::where('title', 'like', '%' . $request->q . '%')
            ->with([
                'author',
                'language',
                'publisher'
            ])->paginate(10);
        $items = [];
        foreach ($models as $model) {
            $items[] = [
                'id' => $model->id,
                'text' => $model->label,
                'result' => View::make('book._autocomplete', ['model' => $model])->render(),
            ];
        }
        return response()->json([
            'total' => $models->total(),
            'per_page' => $models->perPage(),
            'items' => $items,
        ]);
    }

    public function indexCalculation($id) {
        $book = Book::findOrFail($id);

        $channels = Channel::join('channel_groups as cg', 'cg.id', '=', 'channels.channel_group_id')
            ->orderBy('cg.sort')
            ->orderBy('channels.sort')
            ->select('channels.*') // just to avoid fetching anything from joined table
            ->with([
                'channelGroup',
                'mathFunctions'
            ]) // if you need options data anyway
            ->paginate();

        $items = [];
        if ($book->aggregates) {
            $mathEval = new MathEval();
            foreach($channels as $channel) {
                $item = [
                    'name' => $channel->name,
                    'groupName' => $channel->channelGroup->name,
                ];
                foreach($channel->mathFunctions as $mathFunction) {
                    if (!$mathFunction->is_active) continue;
                    $functionItem = [
                        'name' => $mathFunction->name,
                    ];
                    /*
                    $params = [
                        'min_price' => isset($book->aggregates['min_price']) ? $book->aggregates['min_price'] : null,
                        'avg_price' => isset($book->aggregates['avg_price']) ? $book->aggregates['avg_price'] : null,
                        'max_price' => isset($book->aggregates['max_price']) ? $book->aggregates['max_price'] : null,
                    ];
                    */
                    $params = $book->aggregates;

                    if ($channel->channel_group_id == ChannelGroup::GROUP_AMAZON) {
                        $amazonData = $book->amazonData()->where('channel_id', $channel->id)->orderBy('updated_at', 'DESC')->first();
                        if (!$amazonData) continue;
                        $params['rank'] = $amazonData->rank;
                        $params['new_count'] = $amazonData->new_count;
                        $params['new_lowest_price'] = $amazonData->currency->toCurrency($book->book_currency, $amazonData->new_lowest_price);
                        $params['used_count'] = $amazonData->used_count;
                        $params['used_lowest_price'] = $amazonData->currency->toCurrency($book->book_currency, $amazonData->used_lowest_price);
                    }
                    $functionItem['suggested_sale_price'] = $book->book_currency->format($mathEval->execute($mathFunction->expression, $params));
                    $item['functions'][] = $functionItem;
                }
                if (isset($item['functions'])) $items[] = $item;
            }
        }

        return view('book.calculation', [
            'book' => $book,
            'items' => $items
        ]);
    }

    public function recalculate($id) {
        $model = Book::findOrFail($id);
        $model->calcAggregatePrices();
        $model->save();
        return redirect()->route('book.indexCalculation', $id);
    }


}
