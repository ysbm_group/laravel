<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AmazonData;
use App\Book;
use App\Channel;
use DB;
use Session;

class AmazonDataController extends Controller
{

    public function index($book_id) {
        $book = Book::findOrFail($book_id);
        $models = $book->amazonData()->with([
            'currency',
            'channel',
        ])
            ->orderBy('updated_at', 'DESC')
            ->paginate(15);
        //TODO: select distinct channel_id
        return view('amazonData.index', [
            'models' => $models,
            'book' => $book
        ]);
    }

    public function refresh(Request $request, $book_id) {
        $this->validate($request, [
            'channels' => 'required',
        ]);
        $book = Book::findOrFail($book_id);
        set_time_limit(0);

        $result = false;
        DB::beginTransaction();
        try {
            foreach(Channel::whereIn('id', $request->channels)->get() as $channel) {
                $amazonHelper = new \App\Helpers\AmazonHelper($channel);
                if ($book->asin) {
                    $bookData = $amazonHelper->getBookData($book->asin, 'ASIN');
                } else {
                    $bookData = $amazonHelper->getBookData($book->getISBN());
                    if ($bookData) {
                        $book->asin = $bookData['asin'];
                        $book->save();
                    }
                }
                if ($bookData) {
                    $result = $result || $bookData;
                    $amazonData = AmazonData::where('book_id', $book->id)
                        ->where('channel_id', $channel->id)
                        ->where('date', date('Y-m-d'))
                        ->first();
                    if (!$amazonData) {
                        $amazonData = new AmazonData([
                            'book_id' => $book->id,
                            'channel_id' => $channel->id,
                            'date' => date('Y-m-d')
                        ]);
                    }
                    $amazonData->rank = $bookData['rank'];
                    $amazonData->currency_code = isset($bookData['currency']) ? $bookData['currency'] : null;
                    $amazonData->new_count = $bookData['new_count'];
                    if (isset($bookData['new_lowest_price'])) {
                        $amazonData->new_lowest_price = $bookData['new_lowest_price'];
                    }
                    $amazonData->used_count = $bookData['used_count'];
                    if (isset($bookData['used_lowest_price'])) {
                        $amazonData->used_lowest_price = $bookData['used_lowest_price'];
                    }
                    $amazonData->touch();
                    $amazonData->save();
                }
            }
            DB::commit();
            if ($result) Session::flash('success', 'Successfully updated!');
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return redirect()->route('amazonData.index', $book->id);
    }
}
