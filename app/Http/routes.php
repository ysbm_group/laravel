<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('login', ['as'=>'login', 'uses'=>'Auth\AuthController@getLogin']);
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', ['as'=>'logout', 'uses'=>'Auth\AuthController@getLogout']);

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => 'auth'], function() {

    if (config('app.debug')) {
        Route::get('/test', function() {
        });
    }

    Route::get('/', ['as' => 'home', 'uses' => 'AdminController@dashboard']);

    Route::get('users', ['as'=>'user.index', 'uses'=>'UserController@index']);
    Route::get('user/create', ['as'=>'user.create', 'uses'=>'UserController@create']);
    Route::post('user', ['as'=>'user.store', 'uses'=>'UserController@store']);
    Route::get('user/{id}', ['as'=>'user.show', 'uses'=>'UserController@show']);
    Route::get('user/{id}/edit', ['as'=>'user.edit', 'uses'=>'UserController@edit']);
    Route::patch('user/{id}', ['as'=>'user.update', 'uses'=>'UserController@update']);
    Route::delete('user/{id}', ['as'=>'user.destroy', 'uses'=>'UserController@destroy']);

    Route::get('settings', ['as'=>'setting.index', 'uses'=>'SettingController@index']);
    Route::get('setting/{id}', ['as'=>'setting.show', 'uses'=>'SettingController@show']);
    Route::get('setting/{id}/edit', ['as'=>'setting.edit', 'uses'=>'SettingController@edit']);
    Route::patch('setting/{id}', ['as'=>'setting.update', 'uses'=>'SettingController@update']);

    Route::get('authors', ['as'=>'author.index', 'uses'=>'AuthorController@index']);
    Route::get('author/create', ['as'=>'author.create', 'uses'=>'AuthorController@create']);
    Route::post('author', ['as'=>'author.store', 'uses'=>'AuthorController@store']);
    Route::get('author/{id}', ['as'=>'author.show', 'uses'=>'AuthorController@show']);
    Route::get('author/{id}/edit', ['as'=>'author.edit', 'uses'=>'AuthorController@edit']);
    Route::patch('author/{id}', ['as'=>'author.update', 'uses'=>'AuthorController@update']);
    Route::delete('author/{id}', ['as'=>'author.destroy', 'uses'=>'AuthorController@destroy']);
    Route::get('authors/datatables', ['as'=>'author.indexdataTables', 'uses'=>'AuthorController@indexDataTables']);
    Route::get('authors/index-data', ['as'=>'author.indexData', 'uses'=>'AuthorController@indexData']);
    Route::get('author/autocomplete', ['as'=>'author.autoComplete', 'uses'=>'AuthorController@autoComplete']);

    Route::get('languages', ['as'=>'language.index', 'uses'=>'LanguageController@index']);
    Route::get('language/create', ['as'=>'language.create', 'uses'=>'LanguageController@create']);
    Route::post('language', ['as'=>'language.store', 'uses'=>'LanguageController@store']);
    Route::get('language/{id}', ['as'=>'language.show', 'uses'=>'LanguageController@show']);
    Route::get('language/{id}/edit', ['as'=>'language.edit', 'uses'=>'LanguageController@edit']);
    Route::patch('language/{id}', ['as'=>'language.update', 'uses'=>'LanguageController@update']);
    Route::delete('language/{id}', ['as'=>'language.destroy', 'uses'=>'LanguageController@destroy']);

    Route::get('publishers', ['as'=>'publisher.index', 'uses'=>'PublisherController@index']);
    Route::get('publisher/create', ['as'=>'publisher.create', 'uses'=>'PublisherController@create']);
    Route::post('publisher', ['as'=>'publisher.store', 'uses'=>'PublisherController@store']);
    Route::get('publisher/{id}', ['as'=>'publisher.show', 'uses'=>'PublisherController@show']);
    Route::get('publisher/{id}/edit', ['as'=>'publisher.edit', 'uses'=>'PublisherController@edit']);
    Route::patch('publisher/{id}', ['as'=>'publisher.update', 'uses'=>'PublisherController@update']);
    Route::delete('publisher/{id}', ['as'=>'publisher.destroy', 'uses'=>'PublisherController@destroy']);
    Route::get('publisher/autocomplete', ['as'=>'publisher.autoComplete', 'uses'=>'PublisherController@autoComplete']);

    Route::get('tag/autocomplete', ['as'=>'tag.autoComplete', 'uses'=>'TagController@autoComplete']);

    Route::get('currencies', ['as'=>'currency.index', 'uses'=>'CurrencyController@index']);
    Route::get('currency/create', ['as'=>'currency.create', 'uses'=>'CurrencyController@create']);
    Route::post('currency', ['as'=>'currency.store', 'uses'=>'CurrencyController@store']);
    Route::get('currency/{code}', ['as'=>'currency.show', 'uses'=>'CurrencyController@show']);
    Route::get('currency/{code}/edit', ['as'=>'currency.edit', 'uses'=>'CurrencyController@edit']);
    Route::patch('currency/{code}', ['as'=>'currency.update', 'uses'=>'CurrencyController@update']);
    Route::delete('currency/{code}', ['as'=>'currency.destroy', 'uses'=>'CurrencyController@destroy']);

    Route::get('channels', ['as'=>'channel.index', 'uses'=>'ChannelController@index']);
    Route::get('channel/create', ['as'=>'channel.create', 'uses'=>'ChannelController@create']);
    Route::post('channel', ['as'=>'channel.store', 'uses'=>'ChannelController@store']);
    Route::get('channel/{id}', ['as'=>'channel.show', 'uses'=>'ChannelController@show']);
    Route::get('channel/{id}/edit', ['as'=>'channel.edit', 'uses'=>'ChannelController@edit']);
    Route::patch('channel/{id}', ['as'=>'channel.update', 'uses'=>'ChannelController@update']);
    Route::delete('channel/{id}', ['as'=>'channel.destroy', 'uses'=>'ChannelController@destroy']);

    Route::get('books', ['as'=>'book.index', 'uses'=>'BookController@index']);
    Route::get('books/index-data', ['as'=>'book.indexData', 'uses'=>'BookController@indexData']);
    Route::get('book/create', ['as'=>'book.create', 'uses'=>'BookController@create']);
    Route::post('book', ['as'=>'book.store', 'uses'=>'BookController@store']);
    Route::get('book/{id}', ['as'=>'book.show', 'uses'=>'BookController@show']);
    Route::get('book/{id}/add-copy', ['as'=>'book.addCopy', 'uses'=>'BookController@addCopy']);
    Route::get('book/{id}/edit', ['as'=>'book.edit', 'uses'=>'BookController@edit']);

    Route::get('book/{id}/calculations', ['as'=>'book.indexCalculation', 'uses'=>'BookController@indexCalculation']);
    Route::post('book/{id}/recalculate', ['as'=>'book.recalculate', 'uses'=>'BookController@recalculate']);

    Route::patch('book/{id}', ['as'=>'book.update', 'uses'=>'BookController@update']);
    Route::delete('book/{id}', ['as'=>'book.destroy', 'uses'=>'BookController@destroy']);
    Route::get('book/autocomplete', ['as'=>'book.autoComplete', 'uses'=>'BookController@autoComplete']);


    Route::get('book/{book_id}/amazon-data', ['as'=>'amazonData.index', 'uses'=>'AmazonDataController@index']);
    Route::post('book/{book_id}/amazon-data/refresh', ['as'=>'amazonData.refresh', 'uses'=>'AmazonDataController@refresh']);

    Route::get('book/{book_id}/ebay/alerts', ['as'=>'ebayItem.index', 'uses'=>'EbayItemController@index']);
    Route::get('book/{book_id}/ebay/alerts-data', ['as'=>'ebayItem.indexData', 'uses'=>'EbayItemController@indexData']);

    Route::get('book/{book_id}/ebay/live-search', ['as'=>'ebayItem.liveSearch', 'uses'=>'EbayItemController@liveSearch']);
    Route::post('book/{book_id}/ebay/alerts/refresh', ['as'=>'ebayItem.refresh', 'uses'=>'EbayItemController@refresh']);
    Route::get('ebay-item/{id}/edit', ['as'=>'ebayItem.edit', 'uses'=>'EbayItemController@edit']);
    Route::patch('ebay-item/{id}', ['as'=>'ebayItem.update', 'uses'=>'EbayItemController@update']);
    Route::post('ebay-item/{id}/change-status', ['as'=>'ebayItem.changeStatus', 'uses'=>'EbayItemController@changeStatus']);
    Route::post('ebay-items/mass-action', ['as'=>'ebayItem.massAction', 'uses'=>'EbayItemController@massAction']);
    Route::get('ebay-item/{id}', ['as'=>'ebayItem.show', 'uses'=>'EbayItemController@show']);


    Route::get('book/{book_id}/keywords', ['as'=>'keyword.index', 'uses'=>'KeywordController@index']);
    Route::get('book/{book_id}/keyword/create', ['as'=>'keyword.create', 'uses'=>'KeywordController@create']);
    Route::post('book/{book_id}/keyword', ['as'=>'keyword.store', 'uses'=>'KeywordController@store']);
    Route::get('keyword/{id}/edit', ['as'=>'keyword.edit', 'uses'=>'KeywordController@edit']);
    Route::patch('keyword/{id}', ['as'=>'keyword.update', 'uses'=>'KeywordController@update']);
    Route::delete('keyword/{id}', ['as'=>'keyword.destroy', 'uses'=>'KeywordController@destroy']);

    Route::get('conditions', ['as'=>'condition.index', 'uses'=>'ConditionController@index']);
    Route::get('condition/create', ['as'=>'condition.create', 'uses'=>'ConditionController@create']);
    Route::post('condition', ['as'=>'condition.store', 'uses'=>'ConditionController@store']);
    Route::get('condition/{id}', ['as'=>'condition.show', 'uses'=>'ConditionController@show']);
    Route::get('condition/{id}/edit', ['as'=>'condition.edit', 'uses'=>'ConditionController@edit']);
    Route::patch('condition/{id}', ['as'=>'condition.update', 'uses'=>'ConditionController@update']);
    Route::delete('condition/{id}', ['as'=>'condition.destroy', 'uses'=>'ConditionController@destroy']);

    Route::get('locations', ['as'=>'location.index', 'uses'=>'LocationController@index']);
    Route::get('location/create', ['as'=>'location.create', 'uses'=>'LocationController@create']);
    Route::post('location', ['as'=>'location.store', 'uses'=>'LocationController@store']);
    Route::get('location/{id}', ['as'=>'location.show', 'uses'=>'LocationController@show']);
    Route::get('location/{id}/edit', ['as'=>'location.edit', 'uses'=>'LocationController@edit']);
    Route::patch('location/{id}', ['as'=>'location.update', 'uses'=>'LocationController@update']);
    Route::delete('location/{id}', ['as'=>'location.destroy', 'uses'=>'LocationController@destroy']);

    Route::get('copies', ['as'=>'copy.index', 'uses'=>'CopyController@index']);
    Route::get('copies/sold', ['as'=>'copy.indexSold', 'uses'=>'CopyController@indexSold']);
    Route::get('copy/create', ['as'=>'copy.create', 'uses'=>'CopyController@create']);
    Route::post('copy', ['as'=>'copy.store', 'uses'=>'CopyController@store']);
    Route::get('copy/{id}', ['as'=>'copy.show', 'uses'=>'CopyController@show']);
    Route::get('copy/{id}/edit', ['as'=>'copy.edit', 'uses'=>'CopyController@edit']);
    Route::patch('copy/{id}', ['as'=>'copy.update', 'uses'=>'CopyController@update']);
    Route::delete('copy/{id}', ['as'=>'copy.destroy', 'uses'=>'CopyController@destroy']);

    Route::get('copy/{id}/purchase/edit', ['as'=>'copy.purchaseEdit', 'uses'=>'CopyController@purchaseEdit']);
    Route::patch('copy/{id}/purchase', ['as'=>'copy.purchaseUpdate', 'uses'=>'CopyController@purchaseUpdate']);
    Route::get('copy/{id}/sale/edit', ['as'=>'copy.saleEdit', 'uses'=>'CopyController@saleEdit']);
    Route::patch('copy/{id}/sale', ['as'=>'copy.saleUpdate', 'uses'=>'CopyController@saleUpdate']);

    Route::get('schedule-items', ['as'=>'scheduleItem.index', 'uses'=>'ScheduleItemController@index']);
    Route::get('schedule-item/create/{taskType}', ['as'=>'scheduleItem.create', 'uses'=>'ScheduleItemController@create']);
    Route::post('schedule-item/{taskType}', ['as'=>'scheduleItem.store', 'uses'=>'ScheduleItemController@store']);
    Route::get('schedule-item/{id}', ['as'=>'scheduleItem.show', 'uses'=>'ScheduleItemController@show']);
    Route::get('schedule-item/{id}/edit', ['as'=>'scheduleItem.edit', 'uses'=>'ScheduleItemController@edit']);
    Route::patch('schedule-item/{id}', ['as'=>'scheduleItem.update', 'uses'=>'ScheduleItemController@update']);
    Route::delete('schedule-item/{id}', ['as'=>'scheduleItem.destroy', 'uses'=>'ScheduleItemController@destroy']);


    Route::group(['prefix' => '{type}/{functionable_id}'], function($group) {
        Route::get('math-functions', ['as'=>'mathFunction.index', 'uses'=>'MathFunctionController@index']);
        Route::get('math-function/create', ['as'=>'mathFunction.create', 'uses'=>'MathFunctionController@create']);
        Route::post('math-function', ['as'=>'mathFunction.store', 'uses'=>'MathFunctionController@store']);
        foreach($group->getRoutes() as $route){
            $route->where(['type' => '(book|channel)', 'functionable_id' => '[0-9]+']);
        }
    });
    Route::get('math-function/{id}', ['as'=>'mathFunction.show', 'uses'=>'MathFunctionController@show']);
    Route::get('math-function/{id}/edit', ['as'=>'mathFunction.edit', 'uses'=>'MathFunctionController@edit']);
    Route::patch('math-function/{id}', ['as'=>'mathFunction.update', 'uses'=>'MathFunctionController@update']);
    Route::delete('math-function/{id}', ['as'=>'mathFunction.destroy', 'uses'=>'MathFunctionController@destroy']);

});