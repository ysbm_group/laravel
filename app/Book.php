<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {
    use \Conner\Tagging\Taggable;

    protected $fillable = ['isbn_10', 'isbn_13', 'currency_code', 'cover_price', 'language_id', 'publisher_id', 'series',
        'special_type_id', 'year', 'author_id', 'title', 'origin_year', 'origin_title', 'notes',
        'suggested_sale_price', 'price_limit', 'asin', 'aggregates'

    ];

    public function getISBN()
    {
        return $this->isbn_13 ? $this->isbn_13 : ($this->isbn_10 ? $this->isbn_10 : null);
    }

    public function getDecadeAttribute()
    {
        if ($this->year === null) return null;
        $startDecade = $this->year - ($this->year % 10);
        return $startDecade.'-'.($startDecade + 9);
    }

    public function getPriceLimitAttribute()
    {
        //IF MAX SALE PRICE is > EUR 15 = price limit is 35 percent of AVERAGE SALE PRICE
        /*
        if ($this->max_price > 15) {
            return round($this->avg_price / 100 * 35, 2);
        }
        */
        return null;
    }

    public function getLabelAttribute() {
        $label = '"' . $this->title . '"';
        if ($this->author !== null) $label .= ' ' . $this->author->name;
        if ($this->year !== null) $label .= ' (' . $this->year . ')';
        return $label;
    }

    public function setCurrencyCodeAttribute($value)
    {
        $this->attributes['currency_code'] = $value !== '' ? $value : null;
    }

    public function setCoverPriceAttribute($value)
    {
        $this->attributes['cover_price'] = $value !== '' ? $value : null;
    }

    public function setYearAttribute($value)
    {
        $this->attributes['year'] = $value !== '' ? $value : null;
    }

    public function setOriginYearAttribute($value)
    {
        $this->attributes['origin_year'] = $value !== '' ? $value : null;
    }

    public function setLanguageIdAttribute($value)
    {
        $this->attributes['language_id'] = $value !== '' ? $value : null;
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_code', 'code');
    }

    public function getBookCurrencyAttribute() {
        return $this->currency ? $this->currency : Currency::appCurrency();
    }

    public function mathFunctions()
    {
        return $this->morphMany('App\MathFunction', 'functionable');
    }

    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Publisher');
    }

    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function copies()
    {
        return $this->hasMany('App\Copy');
    }

    public function soldCopies()
    {
        return $this->hasMany('App\Copy')
            ->select('copies.*')
            ->join('sales', 'sales.copy_id', '=', 'copies.id');
    }

    public function remainCopies()
    {
        return $this->hasMany('App\Copy')
            ->select('copies.*')
            ->leftJoin('sales', 'sales.copy_id', '=', 'copies.id')
            ->whereNull('sales.id');
    }


    public function keywords() {
        return $this->hasMany('App\Keyword');
    }

    public function amazonData()
    {
        return $this->hasMany('App\AmazonData');
    }

    public function ebayItems()
    {
        return $this->hasMany('App\EbayItem');
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($model) {
            foreach($model->images as $image) {
                //delete as model with clearing attaches
                $image->delete();
            }
        });
    }

    public function calcAggregatePrices() {
        $ebayItems = $this->ebayItems()
            ->approved()
            ->with('currency')
            ->get();
        $count = count($ebayItems);
        $min = $sum = $max = $avg = null;
        $count_1 = $count_2 = 0;
        $sum_1 = $avg_1 = $sum_2 = $avg_2 = $max2 = null;
        foreach($ebayItems as $ebayItem) {
            $bookCurrencyPrice = $ebayItem->currency->toCurrency($this->book_currency, $ebayItem->price);
            $max = max($max, $bookCurrencyPrice);
            $min = $min !== null ? min($min, $bookCurrencyPrice) : $bookCurrencyPrice;
            $sum += $bookCurrencyPrice;

            if ($ebayItem->first_edition) {
                $count_1++;
                $sum_1 += $bookCurrencyPrice;
            } else {
                $count_2++;
                $sum_2 += $bookCurrencyPrice;

                $max2 = max($max2, $bookCurrencyPrice);
            }
        }

        //load to external and than save
        //https://laravel.com/docs/5.1/eloquent-mutators#attribute-casting
        // $aggregates = $this->aggregates;
        // $aggregates['test'] = ...
        // $this->aggregates = $aggregates;

        $this->aggregates = [
            'max_price' => $max,
            'avg_price' => $count ? $sum / $count : null,
            'min_price' => $min,

            'avg_1_price' => $count_1 ? $sum_1 / $count_1 : null,
            'avg_2_price' => $count_2 ? $sum_2 / $count_2 : null,
            'max_2_price' => $max2,
        ];
    }

    protected $casts = [
        'aggregates' => 'array',
    ];

}