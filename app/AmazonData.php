<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonData extends Model
{
    const CREATED_AT = null;
    protected $table = 'amazon_data';

    protected $fillable = [
        'book_id', 'channel_id', 'rank', 'currency_code',
        'new_lowest_price', 'new_count', 'used_lowest_price', 'used_count', 'date'
    ];

    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_code', 'code');
    }

    public function channel()
    {
        return $this->belongsTo('App\Channel');
    }


}
