@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
             ['label' => 'Books', 'link' => route('book.index')],
             ['label' => $book->label, 'link' => route('book.show', $book->id)],
             'Amazon data'
         ]])
    </div>

    <h3>Amazon data</h3>

    <div class="par">
        {!! Form::open(['route' => ['amazonData.refresh', $book->id], 'class' => 'form-inline']) !!}
        <div class="form-group{{ $errors->has('channels') ? ' has-error' : '' }}">
            {!! Form::label('channels', 'Channels', ['class' => 'control-label']) !!}
            {!! Form::select('channels[]', \App\Channel::channelList(false, \App\ChannelGroup::GROUP_AMAZON), null, ['id' => 'channels-list', 'class' => 'form-control', 'multiple' => true, 'style' => 'width: 350px']) !!}
            @if ($errors->has('channels'))
                <span class="help-block">
                    <strong>{{ $errors->first('channels') }}</strong>
                </span>
            @endif
        </div>

        {!! Form::submit('Update from Amazon', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>

    <table class="table table-striped table-bordered">
        <tr>
            <th>Channel</th>
            <th>Sellers New</th>
            <th>Lowest Price New</th>
            <th>Sellers Used</th>
            <th>Lowest Price Used</th>
            <th>Rank</th>
            <th>Date</th>
        </tr>
        <tbody>
        @foreach ($models as $model)
            <tr>
                <td>
                    {{ $model->channel->name }}
                </td>
                <td>
                    {{ $model->new_count }}
                </td>
                <td>
                    {{ $model->currency ? $model->currency->format($model->new_lowest_price) : null }}
                </td>
                <td>
                    {{ $model->used_count }}
                </td>
                <td>
                    {{ $model->currency ?$model->currency->format($model->used_lowest_price) : null }}
                </td>
                <td>
                    {{ $model->rank }}
                </td>
                <td>
                    {{ $model->updated_at }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-5">
            Showing {{ $models->firstItem() }} - {{ $models->lastItem() }} of {{ $models->total() }} records
        </div>
        <div class="col-md-7 text-right">
            {!! $models->render() !!}
        </div>
    </div>
@endsection

@section('page_level_styles')
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $(function() {
            $("#channels-list").select2();
        });
    </script>

@endsection