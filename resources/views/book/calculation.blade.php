@extends('layouts.default')

@section('content')
    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Books', 'link' => route('book.index')],
            ['label' => $book->label, 'link' => route('book.show', $book->id)],
            'Calculations'
        ]])
    </div>

    <h3>Calculations</h3>
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-dark">Ebay aggregates</span>
            </div>
            <div class="actions">
                {!! Form::open(['route' => ['book.recalculate', $book->id]]) !!}
                {!! Form::submit('Recalculate', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="portlet-body">
            @if(isset($book->aggregates))
                <span class="aggregate">Max price <span class="label label-info">{{ $book->book_currency->format( $book->aggregates['max_price']) }}</span></span>
                <span class="aggregate">Avg price <span class="label label-info">{{ $book->book_currency->format( $book->aggregates['avg_price'] ) }}</span></span>

                <span class="aggregate">Max II+ <span class="label label-info">{{ $book->book_currency->format( $book->aggregates['max_2_price']) }}</span></span>
                <span class="aggregate">Average I <span class="label label-info">{{ $book->book_currency->format( $book->aggregates['avg_1_price']) }}</span></span>
                <span class="aggregate">Average II+ <span class="label label-info">{{ $book->book_currency->format( $book->aggregates['avg_2_price']) }}</span></span>
            @endif
        </div>
    </div>

    <table class="table table-striped table-bordered">
        <tr>
            <th>Channel</th>
            <th>Group</th>
            <th>Function</th>
            <th>Suggested sale price</th>
        </tr>
        <tbody>
        @foreach ($items as $item)
            @foreach($item['functions'] as $function)
            <tr>
                <td>
                    {{ $item['name'] }}
                </td>
                <td>
                    {{ $item['groupName'] }}
                </td>
                <td>
                    {{ $function['name'] }}
                </td>
                <td>
                    {{ $function['suggested_sale_price'] }}
                </td>
            </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
@endsection