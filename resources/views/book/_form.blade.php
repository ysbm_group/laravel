<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    @if ($errors->has('title'))
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('series') ? ' has-error' : '' }}">
    {!! Form::label('series', 'Series', ['class' => 'control-label']) !!}
    {!! Form::text('series', null, ['class' => 'form-control']) !!}
    @if ($errors->has('series'))
        <span class="help-block">
            <strong>{{ $errors->first('series') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('author_id') ? ' has-error' : '' }}">
    {!! Form::label('author_id', 'Author', ['class' => 'control-label']) !!}
    <select name="author_id" id="author-id" class="form-control">
        <option value=""></option>
        @if($author)
            <option value="{{$author['key']}}" selected="selected">{{$author['value']}}</option>
        @endif
    </select>
    @if ($errors->has('author_id'))
        <span class="help-block">
            <strong>{{ $errors->first('author_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('language_id') ? ' has-error' : '' }}">
    {!! Form::label('language_id', 'Language', ['class' => 'control-label']) !!}
    {!! Form::select('language_id', \App\Language::languageList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('language_id'))
        <span class="help-block">
            <strong>{{ $errors->first('language_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('publisher_id') ? ' has-error' : '' }}">
    {!! Form::label('publisher_id', 'Publisher', ['class' => 'control-label']) !!}
    <select name="publisher_id" id="publisher-id" class="form-control">
        <option value=""></option>
        @if($publisher)
            <option value="{{$publisher['key']}}" selected="selected">{{$publisher['value']}}</option>
        @endif
    </select>
    @if ($errors->has('publisher_id'))
        <span class="help-block">
            <strong>{{ $errors->first('publisher_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('isbn_10') ? ' has-error' : '' }}">
    {!! Form::label('isbn_10', 'ISBN 10', ['class' => 'control-label']) !!}
    {!! Form::text('isbn_10', null, ['class' => 'form-control']) !!}
    @if ($errors->has('isbn_10'))
        <span class="help-block">
            <strong>{{ $errors->first('isbn_10') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('isbn_13') ? ' has-error' : '' }}">
    {!! Form::label('isbn_13', 'ISBN 13', ['class' => 'control-label']) !!}
    {!! Form::text('isbn_13', null, ['class' => 'form-control']) !!}
    @if ($errors->has('isbn_13'))
        <span class="help-block">
            <strong>{{ $errors->first('isbn_13') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('asin') ? ' has-error' : '' }}">
    {!! Form::label('asin', 'ASIN', ['class' => 'control-label']) !!}
    {!! Form::text('asin', null, ['class' => 'form-control']) !!}
    @if ($errors->has('asin'))
        <span class="help-block">
            <strong>{{ $errors->first('asin') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('currency_code') ? ' has-error' : '' }}">
    {!! Form::label('currency_code', 'Currency', ['class' => 'control-label']) !!}
    {!! Form::select('currency_code', \App\Currency::currencyList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('currency_code'))
        <span class="help-block">
            <strong>{{ $errors->first('currency_code') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('cover_price') ? ' has-error' : '' }}">
    {!! Form::label('cover_price', 'Cover price', ['class' => 'control-label']) !!}
    {!! Form::text('cover_price', null, ['class' => 'form-control']) !!}
    @if ($errors->has('cover_price'))
        <span class="help-block">
            <strong>{{ $errors->first('cover_price') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
    {!! Form::label('year', 'Year', ['class' => 'control-label']) !!}
    {!! Form::text('year', null, ['class' => 'form-control']) !!}
    @if ($errors->has('year'))
        <span class="help-block">
            <strong>{{ $errors->first('year') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('special_type_id') ? ' has-error' : '' }}">
    {!! Form::label('special_type_id', 'Special', ['class' => 'control-label']) !!}
    <input name="special_type_id" type="hidden" value="0">
    {!! Form::checkbox('special_type_id', 1, null, ['class' => 'form-control']) !!}
    @if ($errors->has('special_type_id'))
        <span class="help-block">
            <strong>{{ $errors->first('special_type_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('origin_year') ? ' has-error' : '' }}">
    {!! Form::label('origin_year', 'Original year', ['class' => 'control-label']) !!}
    {!! Form::text('origin_year', null, ['class' => 'form-control']) !!}
    @if ($errors->has('origin_year'))
        <span class="help-block">
            <strong>{{ $errors->first('origin_year') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('origin_title') ? ' has-error' : '' }}">
    {!! Form::label('origin_title', 'Origin title', ['class' => 'control-label']) !!}
    {!! Form::text('origin_title', null, ['class' => 'form-control']) !!}
    @if ($errors->has('origin_title'))
        <span class="help-block">
            <strong>{{ $errors->first('origin_title') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control', 'rows'=> 3]) !!}
    @if ($errors->has('notes'))
        <span class="help-block">
            <strong>{{ $errors->first('notes') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
    {!! Form::label('tags[]', 'Tags', ['class' => 'control-label']) !!}
    <select name="tags[]" id="tags" multiple="true" class="form-control">
        @foreach($tags as $tag)
            <option value="{{$tag}}" selected="selected">{{$tag}}</option>
        @endforeach
    </select>
    @if ($errors->has('tags'))
        <span class="help-block">
            <strong>{{ $errors->first('tags') }}</strong>
        </span>
    @endif
</div>


<div class="panel panel-default">
    <div class="panel-heading">Images</div>
    <div class="panel-body">
        @if(isset($model))
            <div class="row">
                @foreach($model->images as $index => $image)
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <img src="{{ $image->getUrl() }}"/>
                            <div class="caption">
                                <button name="delete_image" value="{{ $image->id }}" class="btn btn-danger confirm" data-confirm="Delete image?" type="submit" >Delete</button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        <div class="form-group{{ $errors->has('images.0') ? ' has-error' : '' }}">
            {!! Form::label('images[]', 'Add images', ['class' => 'control-label']) !!}
            {!! Form::file('images[]', array('multiple'=>true)) !!}
            @if ($errors->has('images.0'))
                <span class="help-block">
            <strong>{{ $errors->first('images.0') }}</strong>
        </span>
            @endif
        </div>

    </div>
</div>

@section('page_level_styles')
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $(function() {

            $("#tags").select2({
                tags: true,
                minimumInputLength: 1,
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                },
                ajax: {
                    url: "{{ route('tag.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                templateResult: function(data) {
                    return data.newOption ? $('<span>"'+data.text+'" (<strong>Add new</strong>)</span>') : data.text;
                }
            });

            $("#author-id").select2({
                //another http://stackoverflow.com/questions/14577014/select2-dropdown-but-allow-new-values-by-user/30021059#30021059
                placeholder: "",
                allowClear: true,
                tags: true,
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                },
                ajax: {
                    url: "{{ route('author.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, //search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * data.per_page) < data.total
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                templateResult: function(data) {
                    return data.newOption ? $('<span>"'+data.text+'" (<strong>Add new</strong>)</span>') : data.text;
                }
            });

            $("#publisher-id").select2({
                placeholder: "",
                allowClear: true,
                //another http://stackoverflow.com/questions/14577014/select2-dropdown-but-allow-new-values-by-user/30021059#30021059
                tags: true,
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                },
                ajax: {
                    url: "{{ route('publisher.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, //search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * data.per_page) < data.total
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                templateResult: function(data) {
                    return data.newOption ? $('<span>"'+data.text+'" (<strong>Add new</strong>)</span>') : data.text;
                }
            });

        } );

    </script>
@endsection

