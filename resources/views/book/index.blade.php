@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            'Books',
        ]])
    </div>

    <h3>Books</h3>

    <p>
        <a href="{{ route('book.create') }}" class="btn btn-success">Create</a>
    </p>
    <table id="datatables-table" class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th style="max-width: 50px">ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Year</th>
            <th>Lang</th>
            <th>Publisher</th>
            <th>Copies</th>
            <th>Remaining copies</th>
            <th>Actions</th>
        </tr>
        <tr class="column-filters">
            <th><input class="form-control"></th>
            <th><input class="form-control"></th>
            <th><select id="author-id" style="min-width: 150px"></select></th>
            <th><input class="form-control"></th>
            <th>{!! Form::select(null, \App\Language::languageList(), null, ['placeholder' => '', 'class'=>'form-control']) !!}</th>
            <th><select id="publisher-id" style="min-width: 150px"></select></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
    </table>
@endsection

@section('page_level_styles')
    <link rel="stylesheet" href="{{ asset('vendor/datatables/css/dataTables.bootstrap.min.css')}}">

    <link href="{{ asset('vendor/datatables/css/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/dataTables.bootstrap.min.js') }}"></script>

    <script src="{{ asset('vendor/datatables/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/jszip.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/js/buttons.html5.min.js') }}"></script>

    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

    <script>
        $(function() {
            $('#datatables-table').DataTable({
                lengthMenu: [[15, 25, 50, 100, -1], [15, 25, 50, 100, "All"]],
                pageLength: 50,
                buttons: [
                    {
                        extend: 'excel',
                        filename: 'Export',
                        exportOptions: {
                            columns: ':not(.actions)'
                        }
                    }
                ],
                processing: true,
                serverSide: true,
                bSortCellsTop: true, //one column header
                //bFilter: false, - disable all filtering
                //bootstrap default
                //"sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                sDom: "<'row'<'col-sm-6'l><'col-sm-6'B>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                ajax: {
                    url: '{!! route('book.indexData') !!}'
                },
                columns: [
                    { data: 'id', name: 'books.id' },
                    { data: 'title', name: 'books.title' },
                    //author.name - from relation, authorS.name - from join
                    { data: 'author_name', name: 'authors.name' },
                    { data: 'year' },
                    { data: 'language_name', name: 'languages.name' },
                    { data: 'publisher_name', name: 'publishers.name' },
                    { data: 'copies_count', name: 'copies_count', orderable: false, searchable: false },
                    { data: 'remain_copies_count', name: 'remain_copies_count', orderable: false, searchable: false },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false, "className": 'actions' }
                ],
                initComplete: function () {
                    var dataTable = this;
                    dataTable.api().columns().every(function () {
                        var column = this;
                        var filterContainer = $(dataTable).find("thead .column-filters th").get(column.index()); //$(column.footer())
                        var filterElement = $(filterContainer).find("input,select");
                        filterElement.on('change', function () {
                            column.search($(this).val() !== null ? $(this).val() : "").draw();
                        });
                    });
                }
                //order: [[1, 'desc']] - default order
            });

            $("#author-id").select2({
                placeholder: "",
                allowClear: true,
                ajax: {
                    url: "{{ route('author.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, //search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * data.per_page) < data.total
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1
            });

            $("#publisher-id").select2({
                placeholder: "",
                allowClear: true,
                ajax: {
                    url: "{{ route('publisher.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, //search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * data.per_page) < data.total
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1
            });
        });
    </script>
@endsection