@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Books', 'link' => route('book.index')],
            'Create',
        ]])
    </div>

    <h3>Create</h3>

    {!! Form::open(['route' => 'book.store', 'files'=>true]) !!}
    @include('book._form')
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@endsection