@extends('layouts.default')

@section('content')
    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Books', 'link' => route('book.index')],
            'Info'
        ]])
    </div>

    <h3>{{ $model->title }}</h3>
    <div class="actions par">
        <a href="{{ route('book.edit', $model->id) }}" class="btn btn-primary">Edit</a>

        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['book.destroy', $model->id]
        ]) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger confirm', 'data-confirm'=>'Delete item?']) !!}
        {!! Form::close() !!}
        <a href="{{ route('book.addCopy', $model->id) }}" class="btn btn-primary">Add a copy</a>

        <a href="{{ route('keyword.index', $model->id) }}" class="btn btn-default">Keywords</a>
        <a href="{{ route('ebayItem.index', $model->id) }}" class="btn btn-default">eBay: Completed search</a>
        <a href="{{ route('ebayItem.liveSearch', $model->id) }}" class="btn btn-default">eBay: Live search</a>
        @if($model->getISBN() || $model->asin)
        <a href="{{ route('amazonData.index', $model->id) }}" class="btn btn-default">Amazon data</a>
        @endif

        <a href="{{ route('book.indexCalculation', $model->id) }}" class="btn btn-default">Calculations</a>
    </div>

    <table class="table table-striped table-bordered">
        <tr>
            <th>ID</th>
            <td>{{ $model->id }}</td>
        </tr>
        <tr>
            <th>ISBN 10</th>
            <td>{{ $model->isbn_10 }}</td>
        </tr>
        <tr>
            <th>ISBN 13</th>
            <td>{{ $model->isbn_13 }}</td>
        </tr>
        <tr>
            <th>ASIN</th>
            <td>{{ $model->asin }}</td>
        </tr>
        <tr>
            <th>Cover price</th>
            <td>{{ $model->cover_price !== null ? $model->currency->format($model->cover_price) : '' }}</td>
        </tr>
        <tr>
            <th>Language</th>
            <td>{{ $model->language ? $model->language->name : null }}</td>
        </tr>
        <tr>
            <th>Publisher</th>
            <td>{{ $model->publisher ? $model->publisher->name : null }}</td>
        </tr>
        <tr>
            <th>Author</th>
            <td>{{ $model->author ? $model->author->name : null }}</td>
        </tr>
        <tr>
            <th>Title</th>
            <td>{{ $model->title }}</td>
        </tr>
        <tr>
            <th>Series</th>
            <td>{{ $model->series }}</td>
        </tr>
        <tr>
            <th>Year</th>
            <td>{{ $model->year }}</td>
        </tr>
        <tr>
            <th>Decade</th>
            <td>{{ $model->decade }}</td>
        </tr>
        <tr>
            <th>Special type</th>
            <td><i class="fa {{ $model->special_type_id ? 'fa-star' : 'fa-star-o' }}"></i></td>
        </tr>
        <tr>
            <th>Original year</th>
            <td>{{ $model->origin_year }}</td>
        </tr>
        <tr>
            <th>Origin title</th>
            <td>{{ $model->origin_title }}</td>
        </tr>
        <tr>
            <th>Notes</th>
            <td>{{ $model->notes }}</td>
        </tr>
        <tr>
            <th>Tags</th>
            <td>{{ implode(', ', $model->tagNames()) }}</td>
        </tr>
        <tr>
            <th>Images</th>
            <td>
                <div class="row">
                    @foreach($model->images as $index => $image)
                        <div class="col-md-3">
                            <div class="thumbnail">
                                <img src="{{ $image->getUrl('thumb') }}"/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </td>
        </tr>
        <tr>
            <th>Created at</th>
            <td>{{ $model->created_at }}</td>
        </tr>
        <tr>
            <th>Update at</th>
            <td>{{ $model->updated_at }}</td>
        </tr>
    </table>

@endsection