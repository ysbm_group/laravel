@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Books', 'link' => route('book.index')],
            'Edit',
        ]])
    </div>

    <h3>Edit</h3>

    {!! Form::model($model, ['method' => 'PATCH', 'route' => ['book.update', $model->id], 'files'=>true]) !!}
    @include('book._form')
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection