<div class="form-group{{ $errors->has('book_id') ? ' has-error' : '' }}">
    {!! Form::label('book_id', 'Book', ['class' => 'control-label']) !!}
    <select name="book_id" id="book-id" class="form-control">
        @if($book)
            <option value="{{$book->id}}" selected="selected">{{$book->label}}</option>
        @endif
    </select>
    @if ($errors->has('book_id'))
        <span class="help-block">
            <strong>{{ $errors->first('book_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
    {!! Form::label('year', 'Year', ['class' => 'control-label']) !!}
    {!! Form::text('year', null, ['class' => 'form-control']) !!}
    @if ($errors->has('year'))
        <span class="help-block">
            <strong>{{ $errors->first('year') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('condition_id') ? ' has-error' : '' }}">
    {!! Form::label('condition_id', 'Condition', ['class' => 'control-label']) !!}
    {!! Form::select('condition_id', \App\Condition::conditionList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('condition_id'))
        <span class="help-block">
            <strong>{{ $errors->first('condition_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
    {!! Form::label('location_id', 'Location', ['class' => 'control-label']) !!}
    {!! Form::select('location_id', \App\Location::locationList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
    @if ($errors->has('location_id'))
        <span class="help-block">
            <strong>{{ $errors->first('location_id') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
    {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control', 'rows'=> 3]) !!}
    @if ($errors->has('notes'))
        <span class="help-block">
            <strong>{{ $errors->first('notes') }}</strong>
        </span>
    @endif
</div>
<div class="panel panel-default">
    <div class="panel-heading">Images</div>
    <div class="panel-body">
        @if(isset($model))
            <div class="row">
                @foreach($model->images as $index => $image)
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <img src="{{ $image->getUrl() }}"/>
                            <div class="caption">
                                <button name="delete_image" value="{{ $image->id }}" class="btn btn-danger confirm" data-confirm="Delete image?" type="submit" >Delete</button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        <div class="form-group{{ $errors->has('images.0') ? ' has-error' : '' }}">
            {!! Form::label('images[]', 'Add images', ['class' => 'control-label']) !!}
            {!! Form::file('images[]', array('multiple'=>true)) !!}
            @if ($errors->has('images.0'))
                <span class="help-block">
            <strong>{{ $errors->first('images.0') }}</strong>
        </span>
            @endif
        </div>

    </div>
</div>

@section('page_level_styles')
    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script>
        $(function() {

            $("#book-id").select2({
                placeholder: "",
                allowClear: true,
                ajax: {
                    url: "{{ route('book.autoComplete') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, //search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * data.per_page) < data.total
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                escapeMarkup: function (markup) { return markup; },
                templateResult: function(repo) {
                    return repo.loading ? repo.text : repo.result;
                }
            });
        });
    </script>

@endsection