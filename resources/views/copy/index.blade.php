@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            'Copies',
        ]])
    </div>

    <h3>Copies</h3>

    <p>
        <a href="{{ route('copy.create') }}" class="btn btn-success">Create</a>
    </p>
    <table class="table table-striped table-bordered">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Year</th>
            <th>Lang</th>
            <th>Condition</th>
            <th>Location</th>
            <th>Actions</th>
        </tr>
        <tbody>
        @foreach ($models as $model)
            <tr>
                <td>
                    {{ $model->id }}
                </td>
                <td>
                    {{ $model->book->title }}
                </td>
                <td>
                    {{ $model->book->author ? $model->book->author->name : null }}
                </td>
                <td>
                    {{ $model->getYear() }}
                </td>
                <td>
                    {{ $model->book->language ? $model->book->language->name : null }}
                </td>
                <td>
                    {{ $model->condition->name }}
                </td>
                <td>
                    {{ $model->location ? $model->location->name : null }}
                </td>
                <td class="actions">
                    <a href="{{ route('copy.show', $model->id) }}" class="btn btn-info">View</a>
                    <a href="{{ route('copy.edit', $model->id) }}" class="btn btn-primary">Edit</a>
                    {!! Form::open([
                       'method' => 'DELETE',
                       'route' => ['copy.destroy', $model->id]
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger confirm', 'data-confirm'=>'Delete item?']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-5">
            Showing {{ $models->firstItem() }} - {{ $models->lastItem() }} of {{ $models->total() }} records
        </div>
        <div class="col-md-7 text-right">
            {!! $models->render() !!}
        </div>
    </div>
@endsection