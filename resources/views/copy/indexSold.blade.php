@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            'Sold copies',
        ]])
    </div>

    <h3>Sold copies</h3>

    <table class="table table-striped table-bordered">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Sale date</th>
            <th>Sale Channel</th>
            <th>Sale price</th>
            <th>Actions</th>
        </tr>
        <tbody>
        @foreach ($models as $model)
            <tr>
                <td>
                    {{ $model->id }}
                </td>
                <td>
                    {{ $model->book->title }}
                </td>
                <td>
                    {{ $model->book->author ? $model->book->author->name : null }}
                </td>
                <td>
                    {{ $model->sale->sale_date }}
                </td>
                <td>
                    {{ $model->sale->channel->name }}
                </td>
                <td>
                    {{ $model->sale->currency->format($model->sale->sale_price) }}
                </td>
                <td class="actions">
                    <a href="{{ route('copy.show', $model->id) }}" class="btn btn-info">View</a>
                    <a href="{{ route('copy.edit', $model->id) }}" class="btn btn-primary">Edit</a>
                    {!! Form::open([
                       'method' => 'DELETE',
                       'route' => ['copy.destroy', $model->id]
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger confirm', 'data-confirm'=>'Delete item?']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-5">
            Showing {{ $models->firstItem() }} - {{ $models->lastItem() }} of {{ $models->total() }} records
        </div>
        <div class="col-md-7 text-right">
            {!! $models->render() !!}
        </div>
    </div>
@endsection