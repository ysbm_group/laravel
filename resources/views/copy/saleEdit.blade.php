@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Copies', 'link' => route('copy.index')],
            ['label' => $copy->label, 'link' => route('copy.show', $copy->id)],
            'Sale',
        ]])
    </div>

    <h3>Edit</h3>

    {!! Form::model($model, ['method' => 'PATCH', 'route' => ['copy.saleUpdate', $copy->id]]) !!}

    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
        {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
        {!! Form::select('status', \App\Sale::statusList(), null, ['class' => 'form-control']) !!}
        @if ($errors->has('status'))
            <span class="help-block">
            <strong>{{ $errors->first('status') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('shipped') ? ' has-error' : '' }}">
        {!! Form::label('shipped', 'Shipped', ['class' => 'control-label']) !!}
        <input name="shipped" type="hidden" value="0">
        {!! Form::checkbox('shipped', 1, null, ['class' => 'form-control']) !!}
        @if ($errors->has('shipped'))
            <span class="help-block">
            <strong>{{ $errors->first('shipped') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('courier') ? ' has-error' : '' }}">
        {!! Form::label('courier', 'Courier', ['class' => 'control-label']) !!}
        {!! Form::text('courier', null, ['class' => 'form-control']) !!}
        @if ($errors->has('courier'))
            <span class="help-block">
            <strong>{{ $errors->first('courier') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('tracking_number') ? ' has-error' : '' }}">
        {!! Form::label('tracking_number', 'Tracking number', ['class' => 'control-label']) !!}
        {!! Form::text('tracking_number', null, ['class' => 'form-control']) !!}
        @if ($errors->has('tracking_number'))
            <span class="help-block">
            <strong>{{ $errors->first('tracking_number') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('first_day_on_sale') ? ' has-error' : '' }}">
        {!! Form::label('first_day_on_sale', 'First day on sale', ['class' => 'control-label']) !!}
        {!! Form::text('first_day_on_sale', null, ['class' => 'form-control']) !!}
        @if ($errors->has('first_day_on_sale'))
            <span class="help-block">
            <strong>{{ $errors->first('first_day_on_sale') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('sale_date') ? ' has-error' : '' }}">
        {!! Form::label('sale_date', 'Sale date', ['class' => 'control-label']) !!}
        {!! Form::text('sale_date', null, ['class' => 'form-control']) !!}
        @if ($errors->has('sale_date'))
            <span class="help-block">
            <strong>{{ $errors->first('sale_date') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('channel_id') ? ' has-error' : '' }}">
        {!! Form::label('channel_id', 'Channel', ['class' => 'control-label']) !!}
        {!! Form::select('channel_id', \App\Channel::channelList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
        @if ($errors->has('channel_id'))
            <span class="help-block">
            <strong>{{ $errors->first('channel_id') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('currency_code') ? ' has-error' : '' }}">
        {!! Form::label('currency_code', 'Currency', ['class' => 'control-label']) !!}
        {!! Form::select('currency_code', \App\Currency::currencyList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
        @if ($errors->has('currency_code'))
            <span class="help-block">
            <strong>{{ $errors->first('currency_code') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('commissions') ? ' has-error' : '' }}">
        {!! Form::label('commissions', 'Commissions', ['class' => 'control-label']) !!}
        {!! Form::text('commissions', null, ['class' => 'form-control']) !!}
        @if ($errors->has('commissions'))
            <span class="help-block">
            <strong>{{ $errors->first('commissions') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('paypal') ? ' has-error' : '' }}">
        {!! Form::label('paypal', 'Paypal', ['class' => 'control-label']) !!}
        <input name="paypal" type="hidden" value="0">
        {!! Form::checkbox('paypal', 1, null, ['class' => 'form-control']) !!}
        @if ($errors->has('paypal'))
            <span class="help-block">
            <strong>{{ $errors->first('paypal') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('paypal_fees') ? ' has-error' : '' }}">
        {!! Form::label('paypal_fees', 'Paypal fees', ['class' => 'control-label']) !!}
        {!! Form::text('paypal_fees', null, ['class' => 'form-control']) !!}
        @if ($errors->has('paypal_fees'))
            <span class="help-block">
            <strong>{{ $errors->first('paypal_fees') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('sale_price') ? ' has-error' : '' }}">
        {!! Form::label('sale_price', 'Sale price', ['class' => 'control-label']) !!}
        {!! Form::text('sale_price', null, ['class' => 'form-control']) !!}
        @if ($errors->has('sale_price'))
            <span class="help-block">
            <strong>{{ $errors->first('sale_price') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('shipping_price') ? ' has-error' : '' }}">
        {!! Form::label('shipping_price', 'Shipping price', ['class' => 'control-label']) !!}
        {!! Form::text('shipping_price', null, ['class' => 'form-control']) !!}
        @if ($errors->has('shipping_price'))
            <span class="help-block">
            <strong>{{ $errors->first('shipping_price') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('actual_shipping_price') ? ' has-error' : '' }}">
        {!! Form::label('actual_shipping_price', 'Actual shipping price', ['class' => 'control-label']) !!}
        {!! Form::text('actual_shipping_price', null, ['class' => 'form-control']) !!}
        @if ($errors->has('actual_shipping_price'))
            <span class="help-block">
            <strong>{{ $errors->first('actual_shipping_price') }}</strong>
        </span>
        @endif
    </div>

    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection

@section('page_level_styles')
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script>
        $(function() {
            $('#first_day_on_sale, #sale_date').datepicker({
                format: 'dd.mm.yyyy',
                weekStart: 1,
                autoclose: true
            });
        });
    </script>
@endsection