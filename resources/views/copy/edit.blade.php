@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Copies', 'link' => route('copy.index')],
            'Edit',
        ]])
    </div>

    <h3>Edit</h3>

    {!! Form::model($model, ['method' => 'PATCH', 'route' => ['copy.update', $model->id], 'files'=>true]) !!}
    @include('copy._form')
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection