@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Copies', 'link' => route('copy.index')],
            'Create',
        ]])
    </div>

    <h3>Create</h3>

    {!! Form::open(['route' => 'copy.store', 'files'=>true]) !!}
    @include('copy._form')
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@endsection