@extends('layouts.default')

@section('content')
    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Copies', 'link' => route('copy.index')],
            'Show'
        ]])
    </div>

    <h3>{{ $model->book->title }}</h3>

    <p>
        <a href="{{ route('copy.edit', $model->id) }}" class="btn btn-primary">Edit</a>
        <a href="{{ route('copy.purchaseEdit', $model->id) }}" class="btn btn-primary">Purchase</a>
        <a href="{{ route('copy.saleEdit', $model->id) }}" class="btn btn-primary">Sale</a>
    </p>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#main" role="tab" data-toggle="tab">Home</a>
        </li>
        @if($model->purchase)
        <li role="presentation">
            <a href="#purchase" role="tab" data-toggle="tab">Purchase</a>
        </li>
        @endif
        @if($model->sale)
            <li role="presentation">
                <a href="#sale" role="tab" data-toggle="tab">Sale</a>
            </li>
        @endif
    </ul>
    <div class="tab-content">
        <div id="main" role="tabpanel" class="tab-pane active">

            <table class="table table-striped table-bordered">
                <tr>
                    <th>ID</th>
                    <td>{{ $model->id }}</td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>{{ $model->book->title }}</td>
                </tr>
                <tr>
                    <th>Author</th>
                    <td>{{ $model->book->author ? $model->book->author->name : null }}</td>
                </tr>
                <tr>
                    <th>Publisher</th>
                    <td>{{ $model->book->publisher ? $model->book->publisher->name : null }}</td>
                </tr>
                <tr>
                    <th>Year</th>
                    <td>{{ $model->getYear() }}</td>
                </tr>
                <tr>
                    <th>Condition</th>
                    <td>{{ $model->condition->name }}</td>
                </tr>
                <tr>
                    <th>Location</th>
                    <td>{{ $model->location ? $model->location->name : null }}</td>
                </tr>
                <tr>
                    <th>Language</th>
                    <td>{{ $model->book->language ? $model->book->language->name : null }}</td>
                </tr>
                <tr>
                    <th>Series</th>
                    <td>{{ $model->book->series }}</td>
                </tr>
                <tr>
                    <th>Notes</th>
                    <td>{{ $model->notes }}</td>
                </tr>
                <tr>
                    <th>Images</th>
                    <td>
                        <div class="row">
                            @foreach($model->images as $index => $image)
                                <div class="col-md-3">
                                    <div class="thumbnail">
                                        <img src="{{ $image->getUrl('thumb') }}"/>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Created at</th>
                    <td>{{ $model->created_at }}</td>
                </tr>
                <tr>
                    <th>Update at</th>
                    <td>{{ $model->updated_at }}</td>
                </tr>
            </table>

        </div>
        @if($model->purchase)
        <div id="purchase" role="tabpanel" class="tab-pane">

            <table class="table table-striped table-bordered">
                <tr>
                    <th>Date</th>
                    <td>{{ $model->purchase->date }}</td>
                </tr>
                <tr>
                    <th>Location</th>
                    <td>{{ $model->purchase->location }}</td>
                </tr>
                <tr>
                    <th>Seller</th>
                    <td>{{ $model->purchase->seller }}</td>
                </tr>
                <tr>
                    <th>Publisher</th>
                    <td>{{ $model->book->publisher ? $model->book->publisher->name : null }}</td>
                </tr>
                <tr>
                    <th>Purchase price</th>
                    <td>{{ $model->purchase->currency->format($model->purchase->purchase_price) }}</td>
                </tr>
                <tr>
                    <th>Shipping price</th>
                    <td>{{ $model->purchase->currency->format($model->purchase->shipping_price) }}</td>
                </tr>
                <tr>
                    <th>Total price</th>
                    <td>{{ $model->purchase->currency->format($model->purchase->total_price) }}</td>
                </tr>
                <tr>
                    <th>Created at</th>
                    <td>{{ $model->created_at }}</td>
                </tr>
                <tr>
                    <th>Update at</th>
                    <td>{{ $model->updated_at }}</td>
                </tr>
            </table>

        </div>
        @endif
        @if($model->sale)
            <div id="sale" role="tabpanel" class="tab-pane">

                <table class="table table-striped table-bordered">
                    <tr>
                        <th>Status</th>
                        <td>{{ $model->sale->getStatusLabel() }}</td>
                    </tr>
                    <tr>
                        <th>Shipped</th>
                        <td>{{ $model->sale->shipped ? 'YES' : '' }}</td>
                    </tr>
                    <tr>
                        <th>Courier</th>
                        <td>{{ $model->sale->courier }}</td>
                    </tr>
                    <tr>
                        <th>Courier</th>
                        <td>{{ $model->sale->courier }}</td>
                    </tr>
                    <tr>
                        <th>Tracking number</th>
                        <td>{{ $model->sale->tracking_number }}</td>
                    </tr>
                    <tr>
                        <th>First day on sale</th>
                        <td>{{ $model->sale->first_day_on_sale }}</td>
                    </tr>
                    <tr>
                        <th>Sale date</th>
                        <td>{{ $model->sale->sale_date }}</td>
                    </tr>
                    <tr>
                        <th>Channel</th>
                        <td>{{ $model->sale->channel ? $model->sale->channel->name : null }}</td>
                    </tr>
                    <tr>
                        <th>Currency</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->name : null }}</td>
                    </tr>
                    <tr>
                        <th>Commissions</th>
                        <td>{{ $model->sale->commissions }}</td>
                    </tr>
                    <tr>
                        <th>PayPal</th>
                        <td>{{ $model->sale->paypal ? 'YES' : '' }}</td>
                    </tr>
                    <tr>
                        <th>PayPal fees</th>
                        <td>{{ $model->sale->paypal_fees }}</td>
                    </tr>
                    <tr>
                        <th>Courier</th>
                        <td>{{ $model->sale->courier }}</td>
                    </tr>
                    <tr>
                        <th>Sale price</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->format($model->sale->sale_price) : null }}</td>
                    </tr>
                    <tr>
                        <th>Shipping price</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->format($model->sale->shipping_price) : null }}</td>
                    </tr>
                    <tr>
                        <th>Actual shipping price</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->format($model->sale->actual_shipping_price) : null }}</td>
                    </tr>
                    <tr>
                        <th>Total sale price</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->format($model->sale->total_sale_price) : null }}</td>
                    </tr>
                    <tr>
                        <th>Cash in</th>
                        <td>{{ $model->sale->currency ? $model->sale->currency->format($model->sale->cash_in) : null }}</td>
                    </tr>
                    <tr>
                        <th>Net profit</th>
                        <td>{{ $model->sale->currency && $model->sale->net_profit ? $model->sale->currency->format($model->sale->net_profit) : null }}</td>
                    </tr>
                    <tr>
                        <th>Created at</th>
                        <td>{{ $model->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Update at</th>
                        <td>{{ $model->updated_at }}</td>
                    </tr>
                </table>

            </div>
        @endif
    </div>

@endsection