@extends('layouts.default')

@section('content')

    <div class="page-bar">
        @include('parts.breadcrumbs', ['breadcrumbs' => [
            ['label' => 'Copies', 'link' => route('copy.index')],
            ['label' => $copy->label, 'link' => route('copy.show', $copy->id)],
            'Purchase',
        ]])
    </div>

    <h3>Edit</h3>

    {!! Form::model($model, ['method' => 'PATCH', 'route' => ['copy.purchaseUpdate', $copy->id]]) !!}

    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
        {!! Form::label('date', 'Date', ['class' => 'control-label']) !!}
        {!! Form::text('date', null, ['class' => 'form-control']) !!}
        @if ($errors->has('date'))
            <span class="help-block">
            <strong>{{ $errors->first('date') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
        {!! Form::label('location', 'Location', ['class' => 'control-label']) !!}
        {!! Form::text('location', null, ['class' => 'form-control']) !!}
        @if ($errors->has('location'))
            <span class="help-block">
            <strong>{{ $errors->first('location') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}">
        {!! Form::label('seller', 'Seller', ['class' => 'control-label']) !!}
        {!! Form::text('seller', null, ['class' => 'form-control']) !!}
        @if ($errors->has('seller'))
            <span class="help-block">
            <strong>{{ $errors->first('seller') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('currency_code') ? ' has-error' : '' }}">
        {!! Form::label('currency_code', 'Currency', ['class' => 'control-label']) !!}
        {!! Form::select('currency_code', \App\Currency::currencyList(), null, ['class' => 'form-control', 'placeholder' => '']) !!}
        @if ($errors->has('currency_code'))
            <span class="help-block">
            <strong>{{ $errors->first('currency_code') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('purchase_price') ? ' has-error' : '' }}">
        {!! Form::label('purchase_price', 'Purchase price', ['class' => 'control-label']) !!}
        {!! Form::text('purchase_price', null, ['class' => 'form-control']) !!}
        @if ($errors->has('purchase_price'))
            <span class="help-block">
            <strong>{{ $errors->first('purchase_price') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('shipping_price') ? ' has-error' : '' }}">
        {!! Form::label('shipping_price', 'Shipping price', ['class' => 'control-label']) !!}
        {!! Form::text('shipping_price', null, ['class' => 'form-control']) !!}
        @if ($errors->has('shipping_price'))
            <span class="help-block">
            <strong>{{ $errors->first('shipping_price') }}</strong>
        </span>
        @endif
    </div>


    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection

@section('page_level_styles')
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page_level_scripts')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script>
        $(function() {
            $('#date').datepicker({
                format: 'dd.mm.yyyy',
                weekStart: 1,
                autoclose: true
            });
        });
    </script>
@endsection